import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'I Am Rich',
      home: Scaffold(
        backgroundColor: Colors.blueGrey[900],
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          title: Text('I Am Rich'),
        ),
        body: SizedBox.expand(
          child: Image.asset(
            'assets/images/diamond.png',
            fit: BoxFit.fitWidth,
          ),
        ),
      ),
    );
  }
}
